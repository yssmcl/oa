from django.conf.urls import url

from .views import NovaMobilizacao

app_name = 'mobilizacao'

urlpatterns = [
    url(r'^novo/$', NovaMobilizacao.as_view(), name='novo'),
]
