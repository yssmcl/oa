from django.shortcuts import render
from django.views.generic.edit import CreateView


class NovaMobilizacao(CreateView):
    def get(self, request):
        return render(request, 'mobilizacao/mobilizacao_form.html')
