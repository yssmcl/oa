from django.apps import AppConfig


class MobilizacaoConfig(AppConfig):
    name = 'mobilizacao'
