from django.core.management.base import BaseCommand
from django.db import transaction

from denuncia.models import AreaDenuncia, CategoriaDenuncia, EstadoDenuncia


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **options):
        self.salvar()
        self.salvar_categorias()

    @staticmethod
    def save_list(class_, list_):
        lista_insercao = []

        for nome in list_:
            if not class_.objects.filter(nome=nome).exists():
                print("Inserindo '{}' em '{}'".format(nome, class_.__name__))
                lista_insercao.append(class_(nome=nome))

        class_.objects.bulk_create(lista_insercao)

    @staticmethod
    def popular_categorias(obj, categorias):
        lista_insercao = []

        for nome in categorias:
            if not AreaDenuncia.objects.filter(nome=nome).exists():
                print("Inserindo '{}' em 'CategoriaDenuncia'".format(nome))
                lista_insercao.append(CategoriaDenuncia(nome=nome, area_denuncia=obj))

        CategoriaDenuncia.objects.bulk_create(lista_insercao)

    def salvar(self):
        areas_denuncia = [
            'Ar',
            'Solos',
            'Vegetação',
            'Animais',
            'Água',
            'Contaminação por agrotóxicos',
        ]

        estados_denuncia = [
            'Pendente (em verificação)',
            'Aceita',
            'Rejeitada',
            'Encaminhada ao órgão competente',
        ]

        self.save_list(AreaDenuncia, areas_denuncia)
        self.save_list(EstadoDenuncia, estados_denuncia)

    def salvar_categorias(self):
        categorias_ar = [
            'Poluição',
            'Cheiro forte',
            'Ruído',
            'Fumaça',
            'Dispersão de agrotóxicos',
        ]

        categorias_solos = [
            'Lixo',
            'Poluição/contaminação',
            'Erosão',
            'Mineração',
        ]

        categorias_vegetacao = [
            'Desmatamento',
            'Queimada',
            'Uso de motosserra',
            'Poda drástica',
            'Árvore doente',
            'Quebra de galhos de árvores devido à passagem de caminhões com altura acima do permitido',
        ]

        categorias_animais = [
            'Caça',
            'Pesca',
            'Tráfico',
            'Cativeiro',
            'Maus tratos',
            'Morte',
            'Foco de dengue',
            'Soltos na pista',
            'Infestação de animais',
        ]

        categorias_agua = [
            'Poluição/contaminação',
            'Aterramento de nascentes',
            'Assoreamento',
            'Morte súbita de peixes',
            'Excesso de cloro na água doméstica',
            'Lixo',
            'Enchente',
        ]

        categorias_contaminacao = [
            'Dispersão aérea',
            'Descarte de embalagens em local inadequado',
            'Valas de contenção',
            'Reutilização de materiais de agrotóxico para outros fins',
        ]

        todas_categorias = []
        todas_categorias.extend((categorias_ar, categorias_solos, categorias_vegetacao, categorias_animais,
                                 categorias_agua, categorias_contaminacao))

        for area, categorias in zip(AreaDenuncia.objects.all(), todas_categorias):
            self.popular_categorias(area, categorias)
