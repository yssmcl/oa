#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
# This file has been automatically generated.
# Instead of changing it, create a file called import_helper.py
# and put there a class called ImportHelper(object) in it.
#
# This class will be specially casted so that instead of extending object,
# it will actually extend the class BasicImportHelper()
#
# That means you just have to overload the methods you want to
# change, leaving the other ones inteact.
#
# Something that you might want to do is use transactions, for example.
#
# Also, don't forget to add the necessary Django imports.
#
# This file was generated with the following command:
# ./manage.py dumpscript denuncia
#
# to restore it, run
# manage.py runscript module_name.this_script_name
#
# example: if manage.py is at ./manage.py
# and the script is at ./some_folder/some_script.py
# you must make sure ./some_folder/__init__.py exists
# and run  ./manage.py runscript some_folder.some_script
import os
import sys
from decimal import Decimal

from django.contrib.contenttypes.models import ContentType
from django.db import transaction


class BasicImportHelper(object):

    def pre_import(self):
        pass

    @transaction.atomic
    def run_import(self, import_data):
        import_data()

    def post_import(self):
        pass

    def locate_similar(self, current_object, search_data):
        # You will probably want to call this method from save_or_locate()
        # Example:
        #   new_obj = self.locate_similar(the_obj, {"national_id": the_obj.national_id } )

        the_obj = current_object.__class__.objects.get(**search_data)
        return the_obj

    def locate_object(self, original_class, original_pk_name, the_class, pk_name, pk_value, obj_content):
        # You may change this function to do specific lookup for specific objects
        #
        # original_class class of the django orm's object that needs to be located
        # original_pk_name the primary key of original_class
        # the_class      parent class of original_class which contains obj_content
        # pk_name        the primary key of original_class
        # pk_value       value of the primary_key
        # obj_content    content of the object which was not exported.
        #
        # You should use obj_content to locate the object on the target db
        #
        # An example where original_class and the_class are different is
        # when original_class is Farmer and the_class is Person. The table
        # may refer to a Farmer but you will actually need to locate Person
        # in order to instantiate that Farmer
        #
        # Example:
        #   if the_class == SurveyResultFormat or the_class == SurveyType or the_class == SurveyState:
        #       pk_name="name"
        #       pk_value=obj_content[pk_name]
        #   if the_class == StaffGroup:
        #       pk_value=8

        search_data = { pk_name: pk_value }
        the_obj = the_class.objects.get(**search_data)
        #print(the_obj)
        return the_obj


    def save_or_locate(self, the_obj):
        # Change this if you want to locate the object in the database
        try:
            the_obj.save()
        except:
            print("---------------")
            print("Error saving the following object:")
            print(the_obj.__class__)
            print(" ")
            print(the_obj.__dict__)
            print(" ")
            print(the_obj)
            print(" ")
            print("---------------")

            raise
        return the_obj


importer = None
try:
    import import_helper
    # We need this so ImportHelper can extend BasicImportHelper, although import_helper.py
    # has no knowlodge of this class
    importer = type("DynamicImportHelper", (import_helper.ImportHelper, BasicImportHelper ) , {} )()
except ImportError as e:
    # From Python 3.3 we can check e.name - string match is for backward compatibility.
    if 'import_helper' in str(e):
        importer = BasicImportHelper()
    else:
        raise


try:
    import dateutil.parser
except ImportError:
    print("Please install python-dateutil")
    sys.exit(os.EX_USAGE)

def run():
    importer.pre_import()
    importer.run_import(import_data)
    importer.post_import()

def import_data():
    # Initial Imports

    # Processing model: denuncia.models.EstadoDenuncia

    from denuncia.models import EstadoDenuncia


    # Processing model: denuncia.models.OrgaoAmbiental

    from denuncia.models import OrgaoAmbiental


    # Processing model: denuncia.models.ProtocoloDenuncia

    from denuncia.models import ProtocoloDenuncia


    # Processing model: denuncia.models.Localizacao

    from denuncia.models import Localizacao


    # Processing model: denuncia.models.GravidadeDenuncia

    from denuncia.models import GravidadeDenuncia


    # Processing model: denuncia.models.Legislacao

    from denuncia.models import Legislacao


    # Processing model: denuncia.models.CategoriaDenuncia

    from denuncia.models import CategoriaDenuncia

    denuncia_categoriadenuncia_1 = CategoriaDenuncia()
    denuncia_categoriadenuncia_1.nome = 'Ar'
    denuncia_categoriadenuncia_1.legislacao = None
    denuncia_categoriadenuncia_1 = importer.save_or_locate(denuncia_categoriadenuncia_1)

    denuncia_categoriadenuncia_2 = CategoriaDenuncia()
    denuncia_categoriadenuncia_2.nome = 'Solos'
    denuncia_categoriadenuncia_2.legislacao = None
    denuncia_categoriadenuncia_2 = importer.save_or_locate(denuncia_categoriadenuncia_2)

    denuncia_categoriadenuncia_3 = CategoriaDenuncia()
    denuncia_categoriadenuncia_3.nome = 'Vegetação'
    denuncia_categoriadenuncia_3.legislacao = None
    denuncia_categoriadenuncia_3 = importer.save_or_locate(denuncia_categoriadenuncia_3)

    denuncia_categoriadenuncia_4 = CategoriaDenuncia()
    denuncia_categoriadenuncia_4.nome = 'Animais'
    denuncia_categoriadenuncia_4.legislacao = None
    denuncia_categoriadenuncia_4 = importer.save_or_locate(denuncia_categoriadenuncia_4)

    denuncia_categoriadenuncia_5 = CategoriaDenuncia()
    denuncia_categoriadenuncia_5.nome = 'Água'
    denuncia_categoriadenuncia_5.legislacao = None
    denuncia_categoriadenuncia_5 = importer.save_or_locate(denuncia_categoriadenuncia_5)

    denuncia_categoriadenuncia_6 = CategoriaDenuncia()
    denuncia_categoriadenuncia_6.nome = 'Contaminação por agrotóxicos'
    denuncia_categoriadenuncia_6.legislacao = None
    denuncia_categoriadenuncia_6 = importer.save_or_locate(denuncia_categoriadenuncia_6)

    # Processing model: denuncia.models.AreaDenuncia

    from denuncia.models import AreaDenuncia

    denuncia_areadenuncia_1 = AreaDenuncia()
    denuncia_areadenuncia_1.nome = 'Poluição'
    denuncia_areadenuncia_1.categoria_denuncia = denuncia_categoriadenuncia_1
    denuncia_areadenuncia_1 = importer.save_or_locate(denuncia_areadenuncia_1)

    denuncia_areadenuncia_2 = AreaDenuncia()
    denuncia_areadenuncia_2.nome = 'Cheiro forte'
    denuncia_areadenuncia_2.categoria_denuncia = denuncia_categoriadenuncia_1
    denuncia_areadenuncia_2 = importer.save_or_locate(denuncia_areadenuncia_2)

    denuncia_areadenuncia_3 = AreaDenuncia()
    denuncia_areadenuncia_3.nome = 'Ruído'
    denuncia_areadenuncia_3.categoria_denuncia = denuncia_categoriadenuncia_1
    denuncia_areadenuncia_3 = importer.save_or_locate(denuncia_areadenuncia_3)

    denuncia_areadenuncia_4 = AreaDenuncia()
    denuncia_areadenuncia_4.nome = 'Fumaça'
    denuncia_areadenuncia_4.categoria_denuncia = denuncia_categoriadenuncia_1
    denuncia_areadenuncia_4 = importer.save_or_locate(denuncia_areadenuncia_4)

    denuncia_areadenuncia_5 = AreaDenuncia()
    denuncia_areadenuncia_5.nome = 'Lixo'
    denuncia_areadenuncia_5.categoria_denuncia = denuncia_categoriadenuncia_2
    denuncia_areadenuncia_5 = importer.save_or_locate(denuncia_areadenuncia_5)

    denuncia_areadenuncia_6 = AreaDenuncia()
    denuncia_areadenuncia_6.nome = 'Poluição/contaminação'
    denuncia_areadenuncia_6.categoria_denuncia = denuncia_categoriadenuncia_2
    denuncia_areadenuncia_6 = importer.save_or_locate(denuncia_areadenuncia_6)

    denuncia_areadenuncia_7 = AreaDenuncia()
    denuncia_areadenuncia_7.nome = 'Erosão'
    denuncia_areadenuncia_7.categoria_denuncia = denuncia_categoriadenuncia_2
    denuncia_areadenuncia_7 = importer.save_or_locate(denuncia_areadenuncia_7)

    denuncia_areadenuncia_8 = AreaDenuncia()
    denuncia_areadenuncia_8.nome = 'Mineração'
    denuncia_areadenuncia_8.categoria_denuncia = denuncia_categoriadenuncia_2
    denuncia_areadenuncia_8 = importer.save_or_locate(denuncia_areadenuncia_8)

    denuncia_areadenuncia_9 = AreaDenuncia()
    denuncia_areadenuncia_9.nome = 'Desmatamento'
    denuncia_areadenuncia_9.categoria_denuncia = denuncia_categoriadenuncia_3
    denuncia_areadenuncia_9 = importer.save_or_locate(denuncia_areadenuncia_9)

    denuncia_areadenuncia_10 = AreaDenuncia()
    denuncia_areadenuncia_10.nome = 'Queimada'
    denuncia_areadenuncia_10.categoria_denuncia = denuncia_categoriadenuncia_3
    denuncia_areadenuncia_10 = importer.save_or_locate(denuncia_areadenuncia_10)

    denuncia_areadenuncia_11 = AreaDenuncia()
    denuncia_areadenuncia_11.nome = 'Uso de motosserra'
    denuncia_areadenuncia_11.categoria_denuncia = denuncia_categoriadenuncia_3
    denuncia_areadenuncia_11 = importer.save_or_locate(denuncia_areadenuncia_11)

    denuncia_areadenuncia_12 = AreaDenuncia()
    denuncia_areadenuncia_12.nome = 'Poda drástica'
    denuncia_areadenuncia_12.categoria_denuncia = denuncia_categoriadenuncia_3
    denuncia_areadenuncia_12 = importer.save_or_locate(denuncia_areadenuncia_12)

    denuncia_areadenuncia_13 = AreaDenuncia()
    denuncia_areadenuncia_13.nome = 'Árvore doente'
    denuncia_areadenuncia_13.categoria_denuncia = denuncia_categoriadenuncia_3
    denuncia_areadenuncia_13 = importer.save_or_locate(denuncia_areadenuncia_13)

    denuncia_areadenuncia_14 = AreaDenuncia()
    denuncia_areadenuncia_14.nome = 'Caça'
    denuncia_areadenuncia_14.categoria_denuncia = denuncia_categoriadenuncia_4
    denuncia_areadenuncia_14 = importer.save_or_locate(denuncia_areadenuncia_14)

    denuncia_areadenuncia_15 = AreaDenuncia()
    denuncia_areadenuncia_15.nome = 'Pesca'
    denuncia_areadenuncia_15.categoria_denuncia = denuncia_categoriadenuncia_4
    denuncia_areadenuncia_15 = importer.save_or_locate(denuncia_areadenuncia_15)

    denuncia_areadenuncia_16 = AreaDenuncia()
    denuncia_areadenuncia_16.nome = 'Tráfico'
    denuncia_areadenuncia_16.categoria_denuncia = denuncia_categoriadenuncia_4
    denuncia_areadenuncia_16 = importer.save_or_locate(denuncia_areadenuncia_16)

    denuncia_areadenuncia_17 = AreaDenuncia()
    denuncia_areadenuncia_17.nome = 'Cativeiro'
    denuncia_areadenuncia_17.categoria_denuncia = denuncia_categoriadenuncia_4
    denuncia_areadenuncia_17 = importer.save_or_locate(denuncia_areadenuncia_17)

    denuncia_areadenuncia_18 = AreaDenuncia()
    denuncia_areadenuncia_18.nome = 'Maus tratos'
    denuncia_areadenuncia_18.categoria_denuncia = denuncia_categoriadenuncia_4
    denuncia_areadenuncia_18 = importer.save_or_locate(denuncia_areadenuncia_18)

    denuncia_areadenuncia_19 = AreaDenuncia()
    denuncia_areadenuncia_19.nome = 'Morte'
    denuncia_areadenuncia_19.categoria_denuncia = denuncia_categoriadenuncia_4
    denuncia_areadenuncia_19 = importer.save_or_locate(denuncia_areadenuncia_19)

    denuncia_areadenuncia_20 = AreaDenuncia()
    denuncia_areadenuncia_20.nome = 'Foco de dengue'
    denuncia_areadenuncia_20.categoria_denuncia = denuncia_categoriadenuncia_4
    denuncia_areadenuncia_20 = importer.save_or_locate(denuncia_areadenuncia_20)

    denuncia_areadenuncia_21 = AreaDenuncia()
    denuncia_areadenuncia_21.nome = 'Soltos na pista'
    denuncia_areadenuncia_21.categoria_denuncia = denuncia_categoriadenuncia_4
    denuncia_areadenuncia_21 = importer.save_or_locate(denuncia_areadenuncia_21)

    denuncia_areadenuncia_22 = AreaDenuncia()
    denuncia_areadenuncia_22.nome = 'Aterramento de nascentes'
    denuncia_areadenuncia_22.categoria_denuncia = denuncia_categoriadenuncia_5
    denuncia_areadenuncia_22 = importer.save_or_locate(denuncia_areadenuncia_22)

    denuncia_areadenuncia_23 = AreaDenuncia()
    denuncia_areadenuncia_23.nome = 'Assoreamento'
    denuncia_areadenuncia_23.categoria_denuncia = denuncia_categoriadenuncia_5
    denuncia_areadenuncia_23 = importer.save_or_locate(denuncia_areadenuncia_23)

    denuncia_areadenuncia_24 = AreaDenuncia()
    denuncia_areadenuncia_24.nome = 'Morte súbita de peixes'
    denuncia_areadenuncia_24.categoria_denuncia = denuncia_categoriadenuncia_5
    denuncia_areadenuncia_24 = importer.save_or_locate(denuncia_areadenuncia_24)

    denuncia_areadenuncia_25 = AreaDenuncia()
    denuncia_areadenuncia_25.nome = 'Excesso de cloro na água doméstica'
    denuncia_areadenuncia_25.categoria_denuncia = denuncia_categoriadenuncia_5
    denuncia_areadenuncia_25 = importer.save_or_locate(denuncia_areadenuncia_25)

    denuncia_areadenuncia_26 = AreaDenuncia()
    denuncia_areadenuncia_26.nome = 'Dispersão aérea'
    denuncia_areadenuncia_26.categoria_denuncia = denuncia_categoriadenuncia_6
    denuncia_areadenuncia_26 = importer.save_or_locate(denuncia_areadenuncia_26)

    denuncia_areadenuncia_27 = AreaDenuncia()
    denuncia_areadenuncia_27.nome = 'Descarte de embalagens em local inadequado'
    denuncia_areadenuncia_27.categoria_denuncia = denuncia_categoriadenuncia_6
    denuncia_areadenuncia_27 = importer.save_or_locate(denuncia_areadenuncia_27)

    denuncia_areadenuncia_28 = AreaDenuncia()
    denuncia_areadenuncia_28.nome = 'Valas de contenção'
    denuncia_areadenuncia_28.categoria_denuncia = denuncia_categoriadenuncia_6
    denuncia_areadenuncia_28 = importer.save_or_locate(denuncia_areadenuncia_28)

    denuncia_areadenuncia_29 = AreaDenuncia()
    denuncia_areadenuncia_29.nome = 'Reutilização de materiais de agrotóxico para outros fins'
    denuncia_areadenuncia_29.categoria_denuncia = denuncia_categoriadenuncia_6
    denuncia_areadenuncia_29 = importer.save_or_locate(denuncia_areadenuncia_29)

    # Processing model: denuncia.models.TipoUsuario

    from denuncia.models import TipoUsuario


    # Processing model: denuncia.models.Usuario

    from denuncia.models import Usuario


    # Processing model: denuncia.models.Denuncia

    from denuncia.models import Denuncia


    # Processing model: denuncia.models.RelatorioDenuncia

    from denuncia.models import RelatorioDenuncia
