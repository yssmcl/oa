$(function() {
	ids = "#id_localizacao-rua, #id_localizacao-bairro, #id_localizacao-cidade"

	// Função de evento para quando o campo cep perde o foco.
	$("#id_localizacao-cep").blur(function() {
		var cep = $(this).val().replace(/\D/g, ""); // somente dígitos

		// Verifica se campo cep possui valor informado e se é válido.
		if (cep != "" && /^[0-9]{8}$/.test(cep)) {
			// Preenche os campos com "..." enquanto consulta WebService.
			$(ids).val("...");

			// Consulta o WebService viacep.com.br
			$.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {
				if (!("erro" in dados)) {
					// Atualiza os campos com os valores da consulta.
					$("#id_localizacao-rua").val(dados.logradouro);
					$("#id_localizacao-bairro").val(dados.bairro);
					$("#id_localizacao-cidade").val(dados.localidade);
				} else { // CEP pesquisado não foi encontrado.
					$(ids).val("");
					alert("CEP não encontrado.");
				}
			});
		} else { // CEP é inválido.
			$(ids).val("");
			alert("Formato de CEP inválido.");
		}
	});
});
