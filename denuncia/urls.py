from django.conf.urls import url

from .views import (
    AceitacaoDenuncia, AtualizacaoDenuncia, ConsultaDenuncia, DetalheDenuncia, EncaminhamentoDenuncia, ExclusaoDenuncia,
    NovaDenuncia, RejeicaoDenuncia)

app_name = 'denuncia'

urlpatterns = [
    url(r'^novo/$', NovaDenuncia.as_view(), name='novo'),
    url(r'^consulta/$', ConsultaDenuncia.as_view(), name='consulta'),
    url(r'^atualizacao/(?P<pk>[0-9]+)/$', AtualizacaoDenuncia.as_view(), name='atualizacao'),
    url(r'^detalhe/$', DetalheDenuncia.as_view(), name='detalhe'),
    url(r'^exclusao/$', ExclusaoDenuncia.as_view(), name='exclusao'),
    url(r'^aceitacao/$', AceitacaoDenuncia.as_view(), name='aceitacao'),
    url(r'^rejeicao/$', RejeicaoDenuncia.as_view(), name='rejeicao'),
    url(r'^encaminhamento/$', EncaminhamentoDenuncia.as_view(), name='encaminhamento'),
]
