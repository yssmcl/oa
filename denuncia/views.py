import os

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.http import urlencode
from django.utils.text import slugify
from django.views.generic import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from settings import UPLOADS_URL

from .forms import AreaDenunciaForm, ArquivoDenuncia, ArquivoDenunciaForm, DenunciaForm, LocalizacaoForm, UsuarioForm
from .models import AreaDenuncia, Denuncia, EstadoDenuncia


def salvar_arquivo_denuncia(arquivo_form, denuncia):
    # Padroniza nome do arquivo e salva
    # arquivo_denuncia = arquivo_form.instance
    arquivo_denuncia = arquivo_form.save(commit=False)
    arquivo_denuncia.denuncia = denuncia
    nome = os.path.splitext(arquivo_denuncia.arquivo.name)[0]
    extensao = os.path.splitext(arquivo_denuncia.arquivo.name)[1]
    arquivo_denuncia.arquivo.name = slugify(nome, allow_unicode=True) + extensao
    arquivo_denuncia.nome = arquivo_denuncia.arquivo.name
    # arquivo_form.save()
    arquivo_denuncia.save()


class NovaDenuncia(CreateView):
    def get(self, request):
        initial_usuario = {
            'nome': 'nome teste',
            'email': 'emailteste@provedor.com',
        }
        initial_localizacao = {
            'cep': '85867-900',
            'rua': 'rua teste',
            'bairro': 'bairro teste',
            'cidade': 'cidade teste',
            'complemento': 'complemento teste',
            'numero': '47',
        }
        initial_area = {
            'nome': AreaDenuncia.objects.get(nome='Ar')
        }

        context = {
            'denuncia_form': DenunciaForm(prefix='denuncia'),
            'usuario_form': UsuarioForm(prefix='usuario', initial=initial_usuario),
            'localizacao_form': LocalizacaoForm(prefix='localizacao', initial=initial_localizacao),
            'area_form': AreaDenunciaForm(prefix='area', initial=initial_area),
            'arquivo_form': ArquivoDenunciaForm(prefix='arquivo'),
            'categoria_selecionada': initial_area['nome'].categoriadenuncia_set.first(),
        }

        return render(request, 'denuncia/denuncia_form.html', context)

    @transaction.atomic
    def post(self, request):
        denuncia_form = DenunciaForm(request.POST, prefix='denuncia')
        usuario_form = UsuarioForm(request.POST, prefix='usuario')
        localizacao_form = LocalizacaoForm(request.POST, prefix='localizacao')
        area_form = AreaDenunciaForm(request.POST, prefix='area')
        arquivo_form = ArquivoDenunciaForm(request.POST, request.FILES, prefix='arquivo')

        # Caso todos os forms da denúncia sejam válidos
        if (denuncia_form.is_valid() and
                usuario_form.is_valid() and
                localizacao_form.is_valid() and
                area_form.is_valid() and
                arquivo_form.is_valid()):
            localizacao = localizacao_form.save()
            usuario = usuario_form.save()

            # Popula os restantes
            denuncia = denuncia_form.save(commit=False)
            denuncia.localizacao = localizacao
            denuncia.usuario = usuario
            denuncia.estado_denuncia = EstadoDenuncia.objects.get(nome='Pendente (em verificação)')
            denuncia.save()

            if len(request.FILES) != 0:
                salvar_arquivo_denuncia(arquivo_form, denuncia)

            messages.success(request, 'Denúncia salva com sucesso.')
            messages.success(request, 'Nº de protocolo: {}'.format(denuncia.pk))
            return redirect('/denuncia/detalhe/?{}'.format(urlencode({'pk': denuncia.pk})))

        # Caso alguns dos forms da denúncia não seja válido
        context = {
            'denuncia_form': denuncia_form,
            'usuario_form': usuario_form,
            'localizacao_form': localizacao_form,
            'area_form': area_form,
            'categoria_selecionada': denuncia_form.instance.categoria_denuncia,
            'arquivo_form': arquivo_form,
        }
        messages.error(request, 'Existem erros em seu formulário.')
        return render(request, 'denuncia/denuncia_form.html', context)


class DetalheDenuncia(DetailView):
    model = Denuncia
    objeto = None

    def get_object(self):
        try:
            pk = self.request.GET.get('pk')
            self.objeto = get_object_or_404(Denuncia, pk=pk)
            return self.objeto
        except Http404:
            messages.error(self.request, 'Denúncia não encontrada.')

    def render_to_response(self, context):
        if not self.objeto:
            return redirect('denuncia:consulta')

        context['UPLOADS_URL'] = UPLOADS_URL
        return super().render_to_response(context)


class ConsultaDenuncia(ListView):
    model = Denuncia

    def get_queryset(self):
        return Denuncia.objects.all()


class AtualizacaoDenuncia(LoginRequiredMixin, UpdateView):
    def get(self, request, pk):
        denuncia = get_object_or_404(Denuncia, pk=pk)
        usuario = denuncia.usuario
        localizacao = denuncia.localizacao

        context = {
            'denuncia_form': DenunciaForm(instance=denuncia, prefix='denuncia'),
            'usuario_form': UsuarioForm(instance=usuario, prefix='usuario'),
            'localizacao_form': LocalizacaoForm(instance=localizacao, prefix='localizacao'),
            'area_form': AreaDenunciaForm(prefix='area', initial={'nome': denuncia.categoria_denuncia.area_denuncia}),
            'categoria_selecionada': denuncia.categoria_denuncia,
        }

        try:
            arquivo_denuncia = ArquivoDenuncia.objects.get(denuncia=denuncia)
            context['arquivo_form'] = ArquivoDenunciaForm(instance=arquivo_denuncia, prefix='arquivo')
        except ArquivoDenuncia.DoesNotExist:
            context['arquivo_form'] = ArquivoDenunciaForm(prefix='arquivo')

        return render(request, 'denuncia/denuncia_form.html', context)

    @transaction.atomic
    def post(self, request, pk):
        denuncia = get_object_or_404(Denuncia, pk=pk)

        denuncia_form = DenunciaForm(request.POST, instance=denuncia, prefix='denuncia')
        usuario_form = UsuarioForm(request.POST, instance=denuncia.usuario, prefix='usuario')
        localizacao_form = LocalizacaoForm(request.POST, instance=denuncia.localizacao, prefix='localizacao')
        area_form = AreaDenunciaForm(request.POST, prefix='area')

        # Checa se a denúncia contém arquivos
        try:
            arquivo_denuncia = ArquivoDenuncia.objects.get(denuncia=denuncia)
            arquivo_form = ArquivoDenunciaForm(request.POST, request.FILES, instance=arquivo_denuncia, prefix='arquivo')
        except ArquivoDenuncia.DoesNotExist:
            arquivo_form = ArquivoDenunciaForm(request.POST, request.FILES, prefix='arquivo')

        if (denuncia_form.is_valid() and
                usuario_form.is_valid() and
                localizacao_form.is_valid() and
                area_form.is_valid() and
                arquivo_form.is_valid()):
            localizacao = localizacao_form.save()
            usuario = usuario_form.save()

            # Popula os restantes
            denuncia = denuncia_form.save(commit=False)
            denuncia.localizacao = localizacao
            denuncia.usuario = usuario
            denuncia.save()

            if len(request.FILES) != 0:
                salvar_arquivo_denuncia(arquivo_form, denuncia)

            messages.success(request, 'Denúncia salva com sucesso.')
            return redirect('denuncia:consulta')

        # Caso alguns dos forms da denúncia não seja válido
        context = {
            'denuncia_form': denuncia_form,
            'usuario_form': usuario_form,
            'localizacao_form': localizacao_form,
            'area_form': area_form,
            'categoria_selecionada': denuncia.categoria_denuncia,
            'arquivo_form': arquivo_form,
        }

        return render(request, 'denuncia/denuncia_form.html', context)


class ExclusaoDenuncia(LoginRequiredMixin, DeleteView):
    def post(self, request):
        denuncia = get_object_or_404(Denuncia, pk=request.POST['pk'])
        # TODO: deletar arquivos relacionados à denúncia
        # arquivo_denuncia = ArquivoDenuncia.objects.get(denuncia=denuncia)
        # num_delecoes_arquivo = arquivo_denuncia.delete()

        num_delecoes_denuncia = denuncia.delete()
        if num_delecoes_denuncia:
            messages.success(request, 'Denúncia removida com sucesso.')
        else:
            messages.error(request, 'Erro na remoção da denúncia.')

        return redirect('denuncia:consulta')


class AceitacaoDenuncia(LoginRequiredMixin, View):
    def post(self, request):
        denuncia = get_object_or_404(Denuncia, pk=request.POST['pk'])

        denuncia.estado_denuncia = EstadoDenuncia.objects.get(nome='Aceita')
        denuncia.save()

        return redirect('denuncia:consulta')


class RejeicaoDenuncia(LoginRequiredMixin, View):
    def post(self, request):
        denuncia = get_object_or_404(Denuncia, pk=request.POST['pk'])

        denuncia.estado_denuncia = EstadoDenuncia.objects.get(nome='Rejeitada')
        denuncia.save()

        return redirect('denuncia:consulta')


class EncaminhamentoDenuncia(LoginRequiredMixin, View):
    def post(self, request):
        denuncia = get_object_or_404(Denuncia, pk=request.POST['pk'])

        denuncia.estado_denuncia = EstadoDenuncia.objects.get(nome='Encaminhada ao órgão competente')
        denuncia.save()

        return redirect('denuncia:consulta')
