from datetime import datetime

from django.core.validators import RegexValidator, validate_image_file_extension
from django.forms import (
    CharField, ClearableFileInput, ImageField, IntegerField, ModelChoiceField, ModelForm, NumberInput, RadioSelect,
    Select, SplitDateTimeField, SplitDateTimeWidget, Textarea, TextInput)

from .models import AreaDenuncia, ArquivoDenuncia, CategoriaDenuncia, Denuncia, Localizacao, Usuario


class DenunciaForm(ModelForm):
    class Meta:
        model = Denuncia
        fields = ['descricao', 'data_ocorrencia', 'categoria_denuncia']

    categoria_denuncia = ModelChoiceField(queryset=CategoriaDenuncia.objects.all(), widget=RadioSelect(), empty_label=None)
    attrs = {'placeholder': 'Descreva os detalhes da ocorrência...'}
    descricao = CharField(max_length=1000, widget=Textarea(attrs=attrs), initial='descrição teste')
    data_ocorrencia = SplitDateTimeField(widget=SplitDateTimeWidget(), initial=datetime.now)

    def clean(self):
        cleaned_data = super(DenunciaForm, self).clean()
        data_ocorrencia = cleaned_data.get('data_ocorrencia')
        categoria_denuncia = cleaned_data.get('categoria_denuncia')

        if not data_ocorrencia:
            self.add_error('email', 'Data inválida.')
        if not categoria_denuncia:
            self.add_error('categoria_denuncia', 'Categoria inválida.')

        return cleaned_data


class UsuarioForm(ModelForm):
    class Meta:
        model = Usuario
        fields = ['nome', 'email']

    nome = CharField(required=False, max_length=100, widget=TextInput())
    email = CharField(required=False, max_length=50, widget=TextInput())

    def clean(self):
        cleaned_data = super(UsuarioForm, self).clean()
        nome = cleaned_data.get('nome')
        email = cleaned_data.get('email')

        if email and not nome:
            self.add_error('nome', 'Se o email foi fornecido, digite também seu nome completo.')
        else:
            cleaned_data['nome'] = nome.upper()

        return cleaned_data


class LocalizacaoForm(ModelForm):
    class Meta:
        model = Localizacao
        fields = ['cep', 'rua', 'bairro', 'cidade', 'complemento', 'numero']

    regex_validator = RegexValidator(r'[0-9]{5}-[0-9]{3}', 'CEP inválido.')
    cep = CharField(max_length=9, widget=TextInput(), validators=[regex_validator])
    rua = CharField(max_length=100, widget=TextInput())
    bairro = CharField(max_length=100, widget=TextInput())
    cidade = CharField(max_length=100, widget=TextInput())
    complemento = CharField(required=False, max_length=100, widget=TextInput())
    numero = IntegerField(required=False, widget=NumberInput())


class AreaDenunciaForm(ModelForm):
    class Meta:
        model = AreaDenuncia
        fields = ['nome']

    attrs = {'class': 'ui dropdown'}
    nome = ModelChoiceField(queryset=AreaDenuncia.objects.all(), widget=Select(attrs=attrs), empty_label='')

    def clean(self):
        cleaned_data = super(AreaDenunciaForm, self).clean()
        nome = cleaned_data.get('nome')

        if not nome:
            self.add_error('nome', 'Área inválida.')

        return cleaned_data


class ArquivoDenunciaForm(ModelForm):
    class Meta:
        model = ArquivoDenuncia
        fields = ['arquivo']

    attrs = {'accept': 'image/*',
             'capture': 'camera',
             'multiple': False}
    arquivo = ImageField(required=False, widget=ClearableFileInput(attrs=attrs), validators=[validate_image_file_extension])
