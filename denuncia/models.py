from django.db.models import (
    CharField, DateTimeField, EmailField, FloatField, ForeignKey, ImageField, IntegerField, Model)

MAX_LENGTH = 100


class EstadoDenuncia(Model):
    nome = CharField(max_length=MAX_LENGTH)

    def __str__(self):
        return self.nome


class Localizacao(Model):
    cep = CharField(max_length=9)
    rua = CharField(max_length=MAX_LENGTH)
    bairro = CharField(max_length=MAX_LENGTH)
    cidade = CharField(max_length=MAX_LENGTH)
    complemento = CharField(max_length=MAX_LENGTH, blank=True, null=True)
    numero = IntegerField(blank=True, null=True)
    pais = CharField(max_length=MAX_LENGTH, blank=True, null=True)
    latitude = FloatField(blank=True, null=True)
    longitude = FloatField(blank=True, null=True)

    def __str__(self):
        return self.cep


# Municipal, estadual, federal etc.
class TipoOrgaoCompetente(Model):
    nome = CharField(max_length=MAX_LENGTH)

    def __str__(self):
        return self.nome


# IAP, IBAMA, ANA etc.
class OrgaoCompetente(Model):
    nome = CharField(max_length=MAX_LENGTH)
    sigla = CharField(max_length=10)
    telefone = CharField(max_length=10)
    email = EmailField(blank=True, null=True)
    site = CharField(max_length=MAX_LENGTH, blank=True, null=True)
    nome_responsavel = CharField(max_length=MAX_LENGTH)

    tipo_orgao = ForeignKey(TipoOrgaoCompetente)
    endereco = ForeignKey(Localizacao)

    def __str__(self):
        return self.nome


class Legislacao(Model):
    nome = CharField(max_length=MAX_LENGTH)
    numero_lei = IntegerField()
    descricao = CharField(max_length=MAX_LENGTH)

    orgao_competente = ForeignKey(OrgaoCompetente)

    def __str__(self):
        return self.nome


class AreaDenuncia(Model):
    nome = CharField(max_length=MAX_LENGTH, help_text='Ar, solos, vegetação, animais, água, contaminação por agrotóxicos')

    legislacao = ForeignKey(Legislacao, blank=True, null=True)

    def __str__(self):
        return self.nome


class CategoriaDenuncia(Model):
    nome = CharField(max_length=MAX_LENGTH, help_text='No caso de ar: poluição, fumaça etc.')

    area_denuncia = ForeignKey(AreaDenuncia)

    def __str__(self):
        return self.nome


# https://github.com/django/djangoproject.com/blob/master/accounts/py
# https://github.com/archlinux/archweb
class Usuario(Model):
    nome = CharField(max_length=MAX_LENGTH)
    email = EmailField()
    data_criacao = DateTimeField(auto_now_add=True)
    # cpf = CharField(max_length=MAX_LENGTH, blank=True, null=True)
    # nome_usuario = CharField(max_length=MAX_LENGTH, blank=True, null=True)
    # senha = CharField(max_length=MAX_LENGTH, blank=True, null=True)
    # salt = CharField(max_length=MAX_LENGTH, blank=True, null=True)
    #  user = OneToOneField(User, related_name='usuario')

    def __str__(self):
        return self.nome


class Denuncia(Model):
    descricao = CharField(max_length=MAX_LENGTH * 10, blank=True, null=True)
    data_ocorrencia = DateTimeField()

    categoria_denuncia = ForeignKey(CategoriaDenuncia)
    localizacao = ForeignKey(Localizacao)
    estado_denuncia = ForeignKey(EstadoDenuncia, blank=True, null=True)
    usuario = ForeignKey(Usuario, blank=True, null=True)  # None/NULL quer dizer que a denúncia é anônima


class ArquivoDenuncia(Model):
    nome = CharField(max_length=MAX_LENGTH)
    arquivo = ImageField(upload_to='uploads/')
    data_envio = DateTimeField(auto_now_add=True)

    denuncia = ForeignKey(Denuncia)

    def __str__(self):
        return self.nome


class RelatorioDenuncia(Model):
    texto = CharField(max_length=MAX_LENGTH)
    data = DateTimeField()

    denuncia = ForeignKey(Denuncia)
    Legislacao = ForeignKey(Legislacao)

    def __str__(self):
        return self.texto
