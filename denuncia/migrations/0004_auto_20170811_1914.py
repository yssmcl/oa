# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-11 19:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('denuncia', '0003_auto_20170811_1749'),
    ]

    operations = [
        migrations.RenameField(
            model_name='areadenuncia',
            old_name='categoria',
            new_name='categoria_denuncia',
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='bairro',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='cidade',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='complemento',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='latitude',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='longitude',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='rua',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
