# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-23 02:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('denuncia', '0016_localizacao_cep'),
    ]

    operations = [
        migrations.AlterField(
            model_name='localizacao',
            name='pais',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
