# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-04 13:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('denuncia', '0014_arquivodenuncia'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ocorrencia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.AlterField(
            model_name='denuncia',
            name='data_ocorrencia',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='denuncia',
            name='localizacao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='denuncia.Localizacao'),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='bairro',
            field=models.CharField(max_length=500),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='cidade',
            field=models.CharField(max_length=500),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='pais',
            field=models.CharField(max_length=500),
        ),
        migrations.AlterField(
            model_name='localizacao',
            name='rua',
            field=models.CharField(max_length=500),
        ),
        migrations.AlterField(
            model_name='orgaoambiental',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
    ]
