# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-10 00:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('denuncia', '0020_auto_20171209_2100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='localizacao',
            name='numero',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
